/**
* Manage contacts
* It will be your responsibility to fine tune this template, add validations, try/catch blocks, logging, etc.
*/
component extends="coldbox.system.EventHandler"{
	
	// DI Virtual Entity Service
	property name="ormService" inject="entityService:ContactOrm";
	
	// HTTP Method Security
	this.allowedMethods = {
		index = "GET", 
		new = "GET", 
		edit = "GET", 
		delete = "POST,DELETE", 
		save = "POST,PUT"
	};
	
	/**
	* preHandler()
	*/
	function preHandler( event, rc, prc ){
		event.paramValue( "format", "html" );
	}
		
	/**
	* Listing
	*/
	function index( event, rc, prc ){
		// Get all contacts
		prc.contacts = ormService.getAll();
		// Multi-format rendering
		event.renderData( data=prc.contacts, formats="xml,json,html,pdf" );
	}	
	
	/**
	* New Form
	*/
	function new( event, rc, prc ){
		// get new ContactOrm
		prc.ContactOrm = ormService.new();
		
		event.setView( "contacts/new" );
	}	

	/**
	* Edit Form
	*/
	function edit( event, rc, prc ){
		// get persisted ContactOrm
		prc.ContactOrm = ormService.get( rc.id );
		
		event.setView( "contacts/edit" );
	}	
	
	/**
	* View ContactOrm mostly used for RESTful services only.
	*/
	function show( event, rc, prc ){
		// Default rendering.
		event.paramValue( "format", "json" );
		// Get requested entity by id
		prc.ContactOrm = ormService.get( rc.id );
		// Multi-format rendering
		event.renderData( data=prc.ContactOrm, formats="xml,json" );
	}

	/**
	* Save and Update
	*/
	function save( event, rc, prc ){
		// get ContactOrm to persist or update and populate it with incoming form
		prc.ContactOrm = populateModel( model=ormService.get( rc.id ), exclude="id", composeRelationships=true );
		
		// Do your validations here
		
		// Save it
		ormService.save( prc.ContactOrm );
		
		// RESTful Handler
		switch(rc.format){
			// xml,json,jsont are by default.  Add your own or remove
			case "xml" : case "json" : case "jsont" :{
				event.renderData( data=prc.ContactOrm, type=rc.format, location="/contacts/show/#prc.ContactOrm.getid()#" );
				break;
			}
			// HTML
			default:{
				// Show a nice notice
				flash.put( "notice", { message="ContactOrm Created", type="success" } );
				// Redirect to listing
				setNextEvent( 'contacts' );
			}
		}
	}	

	/**
	* Delete
	*/
	function delete( event, rc, prc ){
		// Delete record by ID
		var removed = ormService.delete( ormService.get( rc.id ) );
		
		// RESTful Handler
		switch( rc.format ){
			// xml,json,jsont are by default.  Add your own or remove
			case "xml" : case "json" : case "jsont" :{
				var restData = { "deleted" = removed };
				event.renderData( data=restData, type=rc.format );
				break;
			}
			// HTML
			default:{
				// Show a nice notice
				flash.put( "notice", { message="ContactOrm Poofed!", type="success" } );
				// Redirect to listing
				setNextEvent( 'contacts' );
			}
		}
	}	
	
}
