/**
* I am a new handler
*/
component{

	// REST Allowed HTTP Methods Ex: this.allowedMethods = {delete='POST,DELETE',index='GET'}
	this.allowedMethods = {
		init 	= 'Get',
		show 	= 'Get',
		edit 	= 'Get',
		save 	= 'Post, Post',
		delete 	= 'Delete, Post'
	};

	property name="contactService" inject="ContactService";
		
	/**
	* index
	*/
	function index( event, rc, prc, widget = false ) cache="true" cacheTimeout="1"{

		prc.contacts 	= ContactService.getAll();
		prc.widget 		= arguments.widget;
		prc.xehShow 	= 'Contact.show';
		prc.xehEdit 	= 'Contact.edit';
		prc.xehDelete 	= 'Contact.delete';

		if(prc.widget){
			return renderView( "Contact/index" );
		}else{
			event.renderData( 
				data 	= prc.contacts, 
				formats = "json,html,pdf,rss,xml" 
			);
		}
	}

	/**
	* show
	*/
	function show( event, rc, prc ) cache="true" cacheTimeout="1"{
		event.paramValue( "id", 0 );

		prc.id 			= rc.id;
		prc.contact 	= ContactService.getById(prc.id);
		prc.xehIndex 	= 'Contact.index';

		event.setView( "Contact/show" );
	}

	/**
	* edit
	*/
	function edit( event, rc, prc ){
		event.paramValue( "id", 0 );

		prc.id 			= rc.id;
		var contact 	= ContactService.getById(prc.id);

		prc.firstname 	= contact.firstname;
		prc.lastname 	= contact.lastname;
		prc.email 		= contact.email;

		prc.xehIndex 	= 'Contact.index';
		prc.xehSave 	= 'Contact.save';

		event.setView( "Contact/edit" );
	}

	/**
	* save
	*/
	function save( event, rc, prc ){
		prc.contact 	= ContactService.save(rc.id, rc.firstname, rc.lastname, rc.email);

		announceInterception( state="onSave");
		setNextEvent( "Contact.index" );
	}

	/**
	* delete
	*/
	function delete( event, rc, prc ){
		prc.contact 	= ContactService.delete(rc.id);

		announceInterception( state="onDelete");
		setNextEvent( "Contact.index" );
	}


	
}
