/**
* Manage contactTypes
* It will be your responsibility to fine tune this template, add validations, try/catch blocks, logging, etc.
*/
component extends="coldbox.system.EventHandler"{
	
	// DI Virtual Entity Service
	property name="ormService" inject="entityService:ContactType";
	
	// HTTP Method Security
	this.allowedMethods = {
		index = "GET", 
		new = "GET", 
		edit = "GET", 
		delete = "POST,DELETE", 
		save = "POST,PUT"
	};
	
	/**
	* preHandler()
	*/
	function preHandler( event, rc, prc ){
		event.paramValue( "format", "html" );
	}
		
	/**
	* Listing
	*/
	function index( event, rc, prc ){
		// Get all contactTypes
		prc.contactTypes = ormService.getAll();
		// Multi-format rendering
		event.renderData( data=prc.contactTypes, formats="xml,json,html,pdf" );
	}	
	
	/**
	* New Form
	*/
	function new( event, rc, prc ){
		// get new ContactType
		prc.ContactType = ormService.new();
		
		event.setView( "contactTypes/new" );
	}	

	/**
	* Edit Form
	*/
	function edit( event, rc, prc ){
		// get persisted ContactType
		prc.ContactType = ormService.get( rc.id );
		
		event.setView( "contactTypes/edit" );
	}	
	
	/**
	* View ContactType mostly used for RESTful services only.
	*/
	function show( event, rc, prc ){
		// Default rendering.
		event.paramValue( "format", "json" );
		// Get requested entity by id
		prc.ContactType = ormService.get( rc.id );
		// Multi-format rendering
		event.renderData( data=prc.ContactType, formats="xml,json" );
	}

	/**
	* Save and Update
	*/
	function save( event, rc, prc ){
		// get ContactType to persist or update and populate it with incoming form
		prc.ContactType = populateModel( model=ormService.get( rc.id ), exclude="id", composeRelationships=true );
		
		// Do your validations here
		
		// Save it
		ormService.save( prc.ContactType );
		
		// RESTful Handler
		switch(rc.format){
			// xml,json,jsont are by default.  Add your own or remove
			case "xml" : case "json" : case "jsont" :{
				event.renderData( data=prc.ContactType, type=rc.format, location="/contactTypes/show/#prc.ContactType.getid()#" );
				break;
			}
			// HTML
			default:{
				// Show a nice notice
				flash.put( "notice", { message="ContactType Created", type="success" } );
				// Redirect to listing
				setNextEvent( 'contactTypes' );
			}
		}
	}	

	/**
	* Delete
	*/
	function delete( event, rc, prc ){
		// Delete record by ID
		var removed = ormService.delete( ormService.get( rc.id ) );
		
		// RESTful Handler
		switch( rc.format ){
			// xml,json,jsont are by default.  Add your own or remove
			case "xml" : case "json" : case "jsont" :{
				var restData = { "deleted" = removed };
				event.renderData( data=restData, type=rc.format );
				break;
			}
			// HTML
			default:{
				// Show a nice notice
				flash.put( "notice", { message="ContactType Poofed!", type="success" } );
				// Redirect to listing
				setNextEvent( 'contactTypes' );
			}
		}
	}	
	
}
