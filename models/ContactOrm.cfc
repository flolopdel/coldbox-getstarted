/**
* A cool ContactOrm entity
*/
component persistent="true" table="contactorm"{

	// Primary Key
	property name="id" fieldtype="id" column="id" generator="native" setter="false";
	
	// Properties
	property name="firstname" ormtype="string";
	property name="lastname" ormtype="string";
	property name="email" ormtype="string";
	property name="createdate" ormtype="timestamp";

	property name="type" fieldtype="many-to-one" cfc="ContactType" fkcolumn="typeId" fetch="join";
	property name="tags" fieldtype="many-to-many" type="array" lazy="true" orderby="name" cascade="all" cfc="Tag" fkcolumn="fk_contact_id" linktable="ContactTags" inversejoincolumn="fk_tag_id";
	
	
	// Validation
	this.constraints = {
		firstname = { required=true, min="18", type="string" }
	};
	
	// Constructor
	function init(){
		
		return this;
	}
}

