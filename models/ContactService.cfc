/**
* I am a new Model Object
*/
component accessors="true"{
	
	// Properties
	//property name="contacts" type="array";

	/**
	 * Constructor
	 */
	ContactService function init(){

		return this;
	}
	
	/**
	* getAll
	*/
	function getAll(filter = false){

		var contacts = ""; 

		if(filter){

		}else{
			contacts =  queryExecute( "SELECT * FROM contact" );
		}

		return contacts;
		
	}

	/**
	* getById
	*/
	function getById(id){
		
		var contact =  queryExecute( 
			"SELECT * FROM contact where contactID = :contactID ", 
			{contactID = id} 
		);

		return contact
	}

	/**
	* delete
	*/
	function delete(id){

		var contact =  queryExecute( 
			"DELETE FROM contact where contactID = :contactID ", 
			{contactID = id}
		);

		return true;
	}

	/**
	* save
	*/
	function save(id, firstname, lastname, email){

		if(id){

			var contact =  queryExecute( 
				"UPDATE contact  
						SET 
					firstname = :firstname,
					lastname = :lastname,
					email = :email
						WHERE 
					contactID = :contactID", 
				{firstname = firstname, lastname = lastname, email = email, contactID = id}
			);
		}else{

			var contact =  queryExecute( 
				"INSERT INTO contact (firstname, lastname, email) 
						VALUES (:firstname, :lastname, :email)", 
				{firstname = firstname, lastname = lastname, email = email}
			);
		}

		return contact;
	}


}