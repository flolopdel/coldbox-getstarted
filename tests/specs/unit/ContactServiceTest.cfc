/**
* The base model test case will use the 'model' annotation as the instantiation path
* and then create it, prepare it for mocking and then place it in the variables scope as 'model'. It is your
* responsibility to update the model annotation instantiation path and init your model.
*/
component extends="coldbox.system.testing.BaseModelTest" model="models.ContactService"{
	
	/*********************************** LIFE CYCLE Methods ***********************************/

	function beforeAll(){
		// setup the model
		super.setup();
		
		// init the model object
		model.init();
	}

	function afterAll(){
	}

	/*********************************** BDD SUITES ***********************************/
	
	function run(){

		describe( "ContactService Suite", function(){
			
			it( "should retrieve all contacts", function(){
				var contacts = model.getAll();
				expect( contacts ).toBeTypeOf("query");
			});

			it( "should retrieve a contact", function(){
				var contact = model.getById(6);
				expect( contact ).toBeTypeOf("query");
			});

			it( "should add a contact", function(){
				var contact = model.save(0,"Prueba", "Lopez", "admin@prueba.com");
				expect( contact ).toBeTypeOf("query");
			});

			it( "should update a contact", function(){
				var contact = model.save(6,"Prueba", "Lopez", "admin@prueba.com");
				expect( contact ).toBeTypeOf("query");
			});

			it( "should delete a contact", function(){
				var result = model.delete(8);
				expect( result ).toBeTrue();
			});


		});

	}

}
