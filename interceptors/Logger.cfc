/**
* I am a new interceptor
*/
component{
	
	void function configure(){
	
	}
	
	void function preProcess( event, rc, pc, interceptData, buffer ){
		log.info("preProcess");
	}


	void function postProcess( event, rc, pc, interceptData, buffer ){
		log.info("postProcess");
	}


	void function onFooter( event, rc, pc, interceptData, buffer ){
		log.info("onFooter");
		buffer.append("<h1>Florindo López Delgado</h1>");
	}


	void function onSave( event, rc, pc, interceptData, buffer ){
		getCache( 'template' ).clearAll();
		log.info("onSave");
	}


	void function onDelete( event, rc, pc, interceptData, buffer ){
		getCache( 'template' ).clearAll();
		log.info("onDelete " & rc.id);
	}



	
}

