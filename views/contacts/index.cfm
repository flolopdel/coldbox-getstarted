<cfoutput>
<h1>contacts</h1>

<!--- MessageBox --->
<cfif flash.exists( "notice" )>
    <div class="alert alert-#flash.get( "notice" ).type#">
        #flash.get( "notice" ).message#
    </div>
</cfif>

<!--- Create Button --->
#html.href( href="contacts.new", text="Create ContactOrm", class="btn btn-primary")#
#html.br(2)#

<!--- Listing --->

<table class="table table-hover table-striped">
	<thead>
		<tr>
		
			
			<th>tags</th>
		
			
			<th>type</th>
			
		
			
			<th>email</th>
			
		
			
			<th>firstname</th>
			
		
			
		
			
			<th>lastname</th>
			
		
			<th width="150">Actions</th>
		</tr>
	</thead>
	<tbody>
		<cfloop array="#prc.contacts#" index="thisRecord">
		<tr>
			
				
					<td>
						<cfloop array="#thisRecord.gettags()#" index="thisTag">
							#thisTag.getname()#
						</cfloop>
					</td>

				
					<td>#thisRecord.gettype().getname()#</td>
				
			
				
					<td>#thisRecord.getemail()#</td>
				
			
				
					<td>#thisRecord.getfirstname()#</td>
				
			
				
			
				
					<td>#thisRecord.getlastname()#</td>
				
			
			
			<td>
				#html.startForm(action="contacts.delete")#
					#html.hiddenField(name="id", bind=thisRecord)#
					#html.submitButton(value="Delete", onclick="return confirm('Really Delete Record?')", class="btn btn-danger")#
					#html.href(href="contacts.edit", queryString="id=#thisRecord.getid()#", text="Edit", class="btn btn-info")#
				#html.endForm()#
			</td>
		</tr>
		</cfloop>
	</tbody>
</table>

</cfoutput>