<cfoutput>
<h1>tags</h1>

<!--- MessageBox --->
<cfif flash.exists( "notice" )>
    <div class="alert alert-#flash.get( "notice" ).type#">
        #flash.get( "notice" ).message#
    </div>
</cfif>

<!--- Create Button --->
#html.href( href="tags.new", text="Create Tag", class="btn btn-primary")#
#html.br(2)#

<!--- Listing --->

<table class="table table-hover table-striped">
	<thead>
		<tr>
		
			
		
			
			<th>name</th>
			
		
			<th width="150">Actions</th>
		</tr>
	</thead>
	<tbody>
		<cfloop array="#prc.tags#" index="thisRecord">
		<tr>
			
				
			
				
					<td>#thisRecord.getname()#</td>
				
			
			
			<td>
				#html.startForm(action="tags.delete")#
					#html.hiddenField(name="id", bind=thisRecord)#
					#html.submitButton(value="Delete", onclick="return confirm('Really Delete Record?')", class="btn btn-danger")#
					#html.href(href="tags.edit", queryString="id=#thisRecord.getid()#", text="Edit", class="btn btn-info")#
				#html.endForm()#
			</td>
		</tr>
		</cfloop>
	</tbody>
</table>

</cfoutput>