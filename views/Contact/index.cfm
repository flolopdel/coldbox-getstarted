<cfoutput>
<h1>Contact list </h1>
<a href="#event.buildLink(prc.xehEdit)#">Add contact</a>
<div class="row">
	<div class="col-md-12">
		<table class="table table-hover table-striped">
			<thead>
				<tr>
					<th>CONTACTID</th>
					<th>FIRSTNAME</th>
					<th>LASTNAME</th>
					<th>EMAIL</th>
					<th>ACTIONS</th>
				</tr>
			</thead>
			<tbody>
				<cfloop query="#prc.contacts#">
					<tr>
						<td>#prc.contacts.contactID#</td>
						<td>#prc.contacts.firstname#</td>
						<td>#prc.contacts.lastname#</td>
						<td>#prc.contacts.email#</td>
						<td>
							<a class="navbar-brand" href="#event.buildLink( prc.xehShow & '.id.' & prc.contacts.contactID )#">Show</a>
							<a class="navbar-brand" href="#event.buildLink( prc.xehEdit & '.id.' & prc.contacts.contactID )#">Edit</a>
							#html.startForm(action=event.buildLink(prc.xehDelete), method="Delete")#
							#html.hiddenField( name="id", value=prc.contacts.contactID)#
							#html.submitButton( name="delete", value="Delete")#
							#html.endForm()#
						</td>
					</tr>
				</cfloop>
			</tbody>
		</table>
	</div>
</div>
<cfif #prc.widget#>(Widget)</cfif>
</cfoutput>